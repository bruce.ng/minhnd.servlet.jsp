<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- Second of four possible bank-account-display pages that illustrate the MVC approach.-->
<HTML>
<HEAD>
<TITLE>You Owe Us Money!</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap15/bank-support/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
	<TABLE BORDER=5 ALIGN="CENTER">
		<TR><TH CLASS="TITLE">We Know Where You Live!</TH></TR>
	</TABLE>
	<P>
		<IMG SRC="/minhnd.servlet.jsp/chap15/bank-support/Club.gif" ALIGN="LEFT">
		<jsp:useBean id="badCustomer" type="com.servlet.chapter15.BankCustomer" scope="request" />
		Watch out,<jsp:getProperty name="badCustomer" property="firstName" />, we know where you live.
	</P>
	<P>
		Pay us the $<jsp:getProperty name="badCustomer" property="balanceNoSign" />
		you owe us before it is too late!</P>
</BODY>
</HTML>