<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>What's New at JspNews.com</TITLE>
<LINK REL=STYLESHEET
      HREF="JSP-Styles.css"
      TYPE="text/css">
</HEAD>
<BODY>
<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE">What's New at JspNews.com</TH></TR>
</TABLE>
<P>Here is a summary of our three most recent news stories:</P>
<OL>
  <LI><jsp:include page="Item1.jsp" />
  <LI><jsp:include page="Item2.jsp" />
  <LI><jsp:include page="Item3.jsp" />
</OL>
</BODY></HTML>