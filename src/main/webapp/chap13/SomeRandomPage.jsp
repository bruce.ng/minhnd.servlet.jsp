<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- 
Example of including files at page translation time. 
-->
<HTML>
<HEAD>
<TITLE>Some Random Page</TITLE>
<META NAME="author" CONTENT="J. Random Hacker">
<META NAME="keywords" CONTENT="foo,bar,baz,quux">
<META NAME="description" CONTENT="Some random Web page.">
<LINK REL=STYLESHEET HREF="JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <table border=5 align="center">
    <tr>
      <th class="title">some random page</th>
    </tr>
  </table>

  <P>Information about our products and services.</P>
  <P>Blah, blah, blah.</P>
  <P>Yadda, yadda, yadda.</P>
  <!-- <%@ include file="ContactSection.jsp" %> -->
  <jsp:include page="ContactSection.jsp"></jsp:include>
  
</BODY>


<!--
Listing 13.6 shows a page that uses this approach;
Figure 13–2 shows the result.
“Hold on!” you say, “Yes, ContactSection.jsp defines some instance variables (fields).
And, if the main page used those instance variables,
I would agree that you would have to use the include directive.
But, in this particular case, the main page does not use the instance variables,
so jsp:include should be used instead.

Right?” Wrong. If you used jsp:include here, then all the pages that used the footer would see the same access count.
You want each page that uses the footer to maintain a different access count.
You do not want ContactSection.jsp to be its own servlet, you want ContactSection.jsp to provide code that will be part of each separate servlet that results from a JSP page that uses ContactSection.jsp. You need the
include directive. 
 -->
 
<% String destination;
  if (Math.random() > 0.5) {
    destination = "Item1.jsp";
  } else {
    destination = "Item2.jsp";
  }
%>
<!-- <jsp:forward page="<%= destination %>" /> -->
</HTML>