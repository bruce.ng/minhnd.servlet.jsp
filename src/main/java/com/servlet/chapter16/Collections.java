
package com.servlet.chapter16;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





/** Servlet that creates some collections whose elements will
 *  be displayed with the JSP 2.0 expression language.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */
import java.io.IOException;

@WebServlet("/collections")
public class Collections extends HttpServlet {
  
  private static final long serialVersionUID = -232302138741258354L;

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    String[] firstNames = {
      "Bill", "Scott", "Larry"
    };
    ArrayList<String> lastNames = new ArrayList<String>();
    lastNames.add("Ellison");
    lastNames.add("Gates");
    lastNames.add("McNealy");
    
    HashMap<String, String> companyNames = new HashMap<String, String>();
    companyNames.put("Ellison", "Sun");
    companyNames.put("Gates", "Oracle");
    companyNames.put("McNealy", "Microsoft");
    
    request.setAttribute("first", firstNames);
    request.setAttribute("last", lastNames);
    request.setAttribute("company", companyNames);
    
    RequestDispatcher dispatcher = request.getRequestDispatcher("chap16/el/collections.jsp");
    dispatcher.forward(request, response);
  }
}
