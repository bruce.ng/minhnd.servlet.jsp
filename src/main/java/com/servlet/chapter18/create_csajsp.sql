/* SQL command to create an Oracle9i database named csajsp. 
 *
 * Taken from Core Servlets and JavaServer Pages
 * from Prentice Hall and Sun Microsystems Press,
 * http://www.coreservlets.com/.
 * (C) 2003 Marty Hall and Larry Brown;
 * may be freely used or adapted.
 */

CREATE DATABASE csajsp
  USER SYS IDENTIFIED BY csajspDBA
  USER SYSTEM IDENTIFIED BY csajspMAN
  LOGFILE 
    GROUP 1 ('C:\oracle\oradata\csajsp\redo01.log') SIZE 100M,
    GROUP 2 ('C:\oracle\oradata\csajsp\redo02.log') SIZE 100M,
    GROUP 3 ('C:\oracle\oradata\csajsp\redo03.log') SIZE 100M
  MAXLOGFILES 5
  MAXDATAFILES 100
  MAXINSTANCES 1
  CHARACTER SET WE8MSWIN1252
  NATIONAL CHARACTER SET AL16UTF16
  DATAFILE 'C:\oracle\oradata\csajsp\system01.dbf' 
    SIZE 325M REUSE
    AUTOEXTEND ON NEXT 10240K MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL
  DEFAULT TEMPORARY TABLESPACE temp
    TEMPFILE 'C:\oracle\oradata\csajsp\temptbs01.dbf'
    SIZE 20M REUSE 
    EXTENT MANAGEMENT LOCAL
  UNDO TABLESPACE undotbs 
    DATAFILE 'C:\oracle\oradata\csajsp\undotbs01.dbf'
    SIZE 200M REUSE 
    AUTOEXTEND ON NEXT 5120K MAXSIZE UNLIMITED;