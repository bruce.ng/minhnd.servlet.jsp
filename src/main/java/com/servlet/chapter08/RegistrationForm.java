package com.servlet.chapter08;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utility.CookieUtilities;

/** Servlet that displays an HTML form to collect user's
 *  first name, last name, and email address. Uses cookies
 *  to determine the initial values of each of those
 *  form fields.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */

/**
 * Servlet that displays an HTML form to collect user's first name, last name, and email address.
 * Uses cookies to determine the initial values of each of those form fields.
 */
@WebServlet("/registration_form")
public class RegistrationForm extends HttpServlet {
  
  private static final long serialVersionUID = 8752832828205503247L;
  
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String actionURL = "/minhnd.servlet.jsp/registration_servlet2";
    
    String firstName = CookieUtilities.getCookieValue(request, "firstName", "");
    String lastName = CookieUtilities.getCookieValue(request, "lastName", "");
    String emailAddress = CookieUtilities.getCookieValue(request, "emailAddress", "");
    
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    
    String title = "Please Register";
    
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sHtml.append("<CENTER>\n");
    sHtml.append("<H1>" + title + "</H1>\n");
    sHtml.append("<FORM ACTION=\"" + actionURL + "\">\n");
    sHtml.append("First Name:\n" + "  <INPUT TYPE=\"TEXT\" NAME=\"firstName\" " + "VALUE=\"" + firstName + "\"><BR>\n");
    sHtml.append("Last Name:\n" + "  <INPUT TYPE=\"TEXT\" NAME=\"lastName\" " + "VALUE=\"" + lastName + "\"><BR>\n");
    sHtml.append("Email Address: \n" + "  <INPUT TYPE=\"TEXT\" NAME=\"emailAddress\" " + "VALUE=\"" + emailAddress + "\"><BR>\n");
    sHtml.append("<INPUT TYPE=\"SUBMIT\" VALUE=\"Register\">\n");
    sHtml.append("</FORM>");
    sHtml.append("</CENTER>");
    sHtml.append("</BODY>");
    sHtml.append("</HTML>");
    out.println(docType + sHtml.toString());
  }
}
