package com.servlet.chapter08;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** Creates a table of the cookies associated with
 *  the current page. Also sets six cookies: three
 *  that apply only to the current session
 *  (regardless of how long that session lasts)
 *  and three that persist for an hour (regardless
 *  of whether the browser is restarted).
 *  Updated to Java 5.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */
@WebServlet("/cookie_test")
public class CookieTest extends HttpServlet {
  
  private static final long serialVersionUID = 5722001632631007156L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    for (int i = 0; i < 3; i++) {
      // Default maxAge is -1, indicating cookie applies only to current browsing session.
      Cookie cookie = new Cookie("Session-Cookie-" + i, "Cookie-Value-S" + i);
      response.addCookie(cookie);
      cookie = new Cookie("Persistent-Cookie-" + i, "Cookie-Value-P" + i);
      // Cookie is valid for an hour, regardless of whether user quits browser, reboots computer, or whatever.
      cookie.setMaxAge(3600);
      response.addCookie(cookie);
    }
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    String title = "Active Cookies";
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    out.println(docType + sHtml.toString() + "<BODY BGCOLOR=\"#FDF5E6\">\n"
        + "<H1 ALIGN=\"CENTER\">" + title + "</H1>\n" + "<TABLE BORDER=1 ALIGN=\"CENTER\">\n" + "<TR BGCOLOR=\"#FFAD00\">\n"
        + "  <TH>Cookie Name\n" + "  <TH>Cookie Value");
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      out.println("<TR><TH COLSPAN=2>No cookies");
    } else {
      for (Cookie cookie : cookies) {
        out.println("<TR>\n" + "  <TD>" + cookie.getName() + "\n" + "  <TD>" + cookie.getValue());
      }
    }
    out.println("</TABLE></BODY></HTML>");
  }
}
