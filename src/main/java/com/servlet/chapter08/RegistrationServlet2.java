package com.servlet.chapter08;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utility.LongLivedCookie;

/** Reads firstName and lastName request parameters and forwards
 *  to JSP page to display them. Uses session-based bean sharing
 *  to remember previous values.
 */

/** Servlet that processes a registration form containing a user's first name, last name, and email address.
 * If all the values are present, the servlet displays the values.
 * If any of the values are missing, the input form is redisplayed.
 * Either way, the values are put into cookies so that the input form can use the previous values.
 */
@WebServlet("/registration_servlet2")
public class RegistrationServlet2 extends HttpServlet {
  private static final long serialVersionUID = -7072481461874676621L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    boolean isMissingValue = false;
    String firstName = request.getParameter("firstName");
    if (isMissing(firstName)) {
      firstName = "Missing first name";
      isMissingValue = true;
    }
    String lastName = request.getParameter("lastName");
    if (isMissing(lastName)) {
      lastName = "Missing last name";
      isMissingValue = true;
    }
    String emailAddress = request.getParameter("emailAddress");
    if (isMissing(emailAddress)) {
      emailAddress = "Missing email address";
      isMissingValue = true;
    }
    Cookie c1 = new LongLivedCookie("firstName", firstName);
    response.addCookie(c1);
    Cookie c2 = new LongLivedCookie("lastName", lastName);
    response.addCookie(c2);
    Cookie c3 = new LongLivedCookie("emailAddress", emailAddress);
    response.addCookie(c3);
    String formAddress = "/minhnd.servlet.jsp/registration_form";
    if (isMissingValue) {
      response.sendRedirect(formAddress);
    } else {
      PrintWriter out = response.getWriter();
      String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
      String title = "Thanks for Registering";
      StringBuilder sHtml = new StringBuilder();
      sHtml.append("<HTML>\n");
      sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
      sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
      sHtml.append("<CENTER>\n");
      sHtml.append("<H1 ALIGN>" + title + "</H1>\n");
      sHtml.append("<UL>\n");
      sHtml.append("<LI><B>First Name</B>: " + firstName + "\n");
      sHtml.append("<LI><B>Last Name</B>: " + lastName + "\n");
      sHtml.append("<LI><B>Email address</B>: " + emailAddress + "\n");
      sHtml.append("</UL>\n");
      sHtml.append("</CENTER>");
      sHtml.append("</BODY>");
      sHtml.append("</HTML>");
      out.println(docType + sHtml);
    }
  }
  
  /** Determines if value is null or empty. */
  private boolean isMissing(String param) {
    return ((param == null) || (param.trim().equals("")));
  }
}
