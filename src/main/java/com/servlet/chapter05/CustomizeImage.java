package com.servlet.chapter05;

import java.io.*;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/** Servlet that displays referer-specific image.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */
@WebServlet("/customizeImage")
public class CustomizeImage extends HttpServlet {
  
  /**
   * 
   */
  private static final long serialVersionUID = 5929751864396642182L;
  
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String referer = request.getHeader("Referer");
    if (referer == null) {
      referer = "<I>none</I>";
    }
    String title = "Referring page: " + referer;
    String imageName;
    
    if (contains(referer, "JRun")) {
      imageName = "jrun-powered.png";
    } else if (contains(referer, "Resin")) {
      imageName = "resin-powered.gif";
    } else {
      imageName = "tomcat-powered.jpg";
    }
    
    String imagePath = "/minhnd.servlet.jsp/images/" + imageName;
    StringBuilder sDocType = new StringBuilder();
    sDocType.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 ");
    sDocType.append("Transitional//EN\">\n");
    sDocType.append("<HTML>\n");
    sDocType.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sDocType.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sDocType.append("<CENTER><H2>" + title + "</H2>\n");
    sDocType.append("<IMG SRC=\"" + imagePath + "\">\n");
    sDocType.append("</CENTER></BODY></HTML>");
    sDocType.append("");
    sDocType.append("");
    sDocType.append("");
    out.println(sDocType.toString());
  }
  
  private boolean contains(String mainString, String subString) {
    return (mainString.indexOf(subString) != -1);
  }
}
