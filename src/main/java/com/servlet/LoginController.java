package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.UserDAO;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
  
  private static final long serialVersionUID = 1L;
  
  public LoginController() {
    super();
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if ("logout".equalsIgnoreCase(request.getParameter("param"))) {
      HttpSession session = request.getSession();
      session.removeAttribute("user");
      session.invalidate();
      response.sendRedirect("index.jsp");
    }
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //response.setContentType("text/html");
    String error;
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    HttpSession session = request.getSession();
    UserDAO userDAO = new UserDAO();
    String userName = userDAO.verifyUser(email, password);
    
    if (userName == null) {
      error = "Invalid Email or password";
      session.setAttribute("error", error);
      response.sendRedirect("index.jsp");
    } else {
      session.setAttribute("user", userName);
      response.sendRedirect("welcome.jsp");
    }
  }
}
