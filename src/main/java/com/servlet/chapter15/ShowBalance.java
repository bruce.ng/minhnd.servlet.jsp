
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** Servlet that reads a customer ID and displays
 *  information on the account balance of the customer
 *  who has that ID.
 */

@WebServlet("/show_balance")
public class ShowBalance extends HttpServlet {
  
  private static final long serialVersionUID = -6935057189934160290L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    BankCustomer customer = BankCustomer.getCustomer(request.getParameter("id"));
    String address;
    if (customer == null) {
      address = "/chap15/bank-account/UnknownCustomer.jsp";
    } else if (customer.getBalance() < 0) {
      address = "/chap15/bank-account/NegativeBalance.jsp";
      request.setAttribute("badCustomer", customer);
    } else if (customer.getBalance() < 10000) {
      address = "/chap15/bank-account/NormalBalance.jsp";
      request.setAttribute("regularCustomer", customer);
    } else {
      address = "/chap15/bank-account/HighBalance.jsp";
      request.setAttribute("eliteCustomer", customer);
    }
    RequestDispatcher dispatcher = request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
