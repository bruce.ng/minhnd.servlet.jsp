
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/chap15_prime_servlet")
public class PrimeServlet extends HttpServlet {
  
  private static final long serialVersionUID = 1790969779306732059L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    String length = request.getParameter("primeLength");
    ServletContext context = getServletContext();
    synchronized (this) {
      if ((context.getAttribute("primeBean") == null) || (length != null)) {
        PrimeBean primeBean = new PrimeBean(length);
        context.setAttribute("primeBean", primeBean);
      }
      //String address = "/WEB-INF/mvc-sharing/ShowPrime.jsp";
      String address = "/chap15/mvc-sharing/ShowPrime.jsp";
      RequestDispatcher dispatcher = request.getRequestDispatcher(address);
      dispatcher.forward(request, response);
    }
  }
}
