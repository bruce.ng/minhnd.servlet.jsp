
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/including_mutli_pages")
public class IncludingMutliPages extends HttpServlet {
  
  private static final long serialVersionUID = 1790969779306732059L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    String firstTable, secondTable, thirdTable;
    if (true) {
      firstTable = "/chap15/Canel.jsp";
      secondTable = "/chap15/Order.jsp";
      thirdTable = "/chap15/UnknownOperation.jsp";
    }
    //else if (...) { ... }
    RequestDispatcher dispatcher = request.getRequestDispatcher("/chap15/Header.jsp");
    dispatcher.include(request, response);
    dispatcher = request.getRequestDispatcher(firstTable);
    dispatcher.include(request, response);
    dispatcher = request.getRequestDispatcher(secondTable);
    dispatcher.include(request, response);
    dispatcher = request.getRequestDispatcher(thirdTable);
    dispatcher.include(request, response);
    dispatcher = request.getRequestDispatcher("/chap15/Footer.jsp");
    dispatcher.include(request, response);
  }
}
/**
 * The forward method of RequestDispatcher relies on the destination JSP page to generate the complete output.
 * The servlet is not permitted to generate any output of its own.
 * An alternative to forward is include .
 * With include , the servlet can combine its output with that of one or more JSP pages.
 * More commonly, the servlet still relies on JSP pages to produce the output, but the servlet invokes different
 * JSP pages to create different sections of the page.
 * Does this sound familiar? It should: the include method of RequestDispatcher is the code that the jsp:include 
 * action (Section 13.1) invokes behind the scenes.
 * 
 * This approach is most common when your servlets create portal sites that let users specify where on the page
 * they want various pieces of content to be displayed
 * */
