
package com.servlet.chapter17;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** A simple servlet that connects to a database and
 *  presents the results from the query in an HTML
 *  table. The driver, URL, username, password,
 *  and query are taken from form input parameters.
 */

@WebServlet("/northwind_servlet")
public class NorthwindServlet extends HttpServlet {
  
  private static final long serialVersionUID = -4469672602976063751L;

  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\"\n";
    String title = "Northwind Results";
    out.print(docType + "<HTML>\n" + "<HEAD><TITLE>" + title + "</TITLE></HEAD>\n" + "<BODY BGCOLOR=\"#FDF5E6\"><CENTER>\n" +
      "<H1>Database Results</H1>\n");
    String driver = request.getParameter("driver");
    String url = request.getParameter("url");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String query = request.getParameter("query");
    showTable(driver, url, username, password, query, out);
    out.println("</CENTER></BODY></HTML>");
  }
  
  public void showTable(String driver, String url, String username, String password, String query, PrintWriter out) {
    try {
      // Load database driver if it's not already loaded.
      Class.forName(driver);
      // Establish network connection to database.
      Connection connection = DriverManager.getConnection(url, username, password);
      // Look up info about the database as a whole.
      DatabaseMetaData dbMetaData = connection.getMetaData();
      out.println("<UL>");
      String productName = dbMetaData.getDatabaseProductName();
      String productVersion = dbMetaData.getDatabaseProductVersion();
      out.println("  <LI><B>Database:</B> " + productName + "  <LI><B>Version:</B> " + productVersion + "</UL>");
      Statement statement = connection.createStatement();
      // Send query to database and store results.
      ResultSet resultSet = statement.executeQuery(query);
      // Print results.
      out.println("<TABLE BORDER=1>");
      ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
      int columnCount = resultSetMetaData.getColumnCount();
      out.println("<TR>");
      // Column index starts at 1 (a la SQL), not 0 (a la Java).
      for (int i = 1; i <= columnCount; i++) {
        out.print("<TH>" + resultSetMetaData.getColumnName(i));
      }
      out.println();
      // Step through each row in the result set.
      while (resultSet.next()) {
        out.println("<TR>");
        // Step across the row, retrieving the data in each
        // column cell as a String.
        for (int i = 1; i <= columnCount; i++) {
          out.print("<TD>" + resultSet.getString(i));
        }
        out.println();
      }
      out.println("</TABLE>");
      connection.close();
    } catch (ClassNotFoundException cnfe) {
      System.err.println("Error loading driver: " + cnfe);
    } catch (SQLException sqle) {
      System.err.println("Error connecting: " + sqle);
    } catch (Exception ex) {
      System.err.println("Error with input: " + ex);
    }
  }
  
  private static void showResults(ResultSet results)
    throws SQLException {
    while (results.next()) {
      System.out.print(results.getString(1) + " ");
    }
    System.out.println();
  }
  
  private static void printUsage() {
    System.out.println("Usage: PreparedStatements host " + "dbName username password " + "vendor [print].");
  }
}
