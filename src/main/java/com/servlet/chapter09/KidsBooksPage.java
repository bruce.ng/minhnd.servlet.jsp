package com.servlet.chapter09;

import javax.servlet.annotation.WebServlet;

/** A specialization of the CatalogPage servlet that displays a page selling three famous kids-book series.
 *  Orders are sent to the OrderPage servlet.
 */

@WebServlet("/kids_books_page")
public class KidsBooksPage extends CatalogPage {
  private static final long serialVersionUID = -7671529983007354603L;
  
  public void init() {
    String[] ids = { "lewis001", "alexander001", "rowling001" };
    setItems(ids);
    setTitle("All-Time Best Children's Fantasy Books");
  }
}
