package com.servlet.chapter09;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/** Servlet that displays a list of items being ordered.
 *  Accumulates them in an ArrayList with no attempt at
 *  detecting repeated items.
 *  Used to demonstrate basic session tracking.
 */

@SuppressWarnings("unchecked")
@WebServlet("/show_items")
public class ShowItems extends HttpServlet {
  
  private static final long serialVersionUID = 4292768950974040362L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    ArrayList<String> previousItems = (ArrayList<String>) session.getAttribute("previousItems");
    if (previousItems == null) {
      previousItems = new ArrayList<String>();
      session.setAttribute("previousItems", previousItems);
    }
    String newItem = request.getParameter("newItem");
    if ((newItem != null) && (!newItem.trim().equals(""))) {
      previousItems.add(newItem);
    }
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Items Purchased";
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    out.println(docType + "<HTML>\n" + "<HEAD><TITLE>" + title + "</TITLE></HEAD>\n" + "<BODY BGCOLOR=\"#FDF5E6\">\n" + "<H1>"
        + title + "</H1>");
    if (previousItems.size() == 0) {
      out.println("<I>No items</I>");
    } else {
      out.println("<UL>");
      for (String item : previousItems) {
        out.println("  <LI>" + item);
      }
      out.println("</UL>");
    }
    out.println("</BODY></HTML>");
  }
}
