package com.model;

public class AirPollution {
  
  private String Source;
  private int Percentage;
  
  public AirPollution() {
  }
  
  public AirPollution(String source, int percentage) {
    Source = source;
    Percentage = percentage;
  }
  
  public String getSource() {
    return Source;
  }
  
  public int getPercentage() {
    return Percentage;
  }
  
  public void setSource(String source) {
    Source = source;
  }
  
  public void setPercentage(int percentage) {
    Percentage = percentage;
  }
}
