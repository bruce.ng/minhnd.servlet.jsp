package com.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.model.AirPollution;
import com.model.RevenueReport;
import com.opencsv.CSVReader;

public class BusinessService {
  
  public static List<RevenueReport> getCompanyList() {
    
    List<RevenueReport> listOfCompany = new LinkedList<RevenueReport>();
    
    listOfCompany.add(new RevenueReport("Bosch", "Germany", 2012, 40000));
    listOfCompany.add(new RevenueReport("XYZ", "India", 2014, 10000));
    listOfCompany.add(new RevenueReport("Robotics", "United Kingdom", 2011, 35000));
    listOfCompany.add(new RevenueReport("Merch", "USA", 2010, 20000));
    listOfCompany.add(new RevenueReport("Foxtron", "Indonesia", 2009, 15000));
    listOfCompany.add(new RevenueReport("Benz", "Germany", 2013, 50000));
    listOfCompany.add(new RevenueReport("Audi", "United Kingdom", 2012, 60000));
    listOfCompany.add(new RevenueReport("Hyat", "France", 2011, 30000));
    listOfCompany.add(new RevenueReport("HCL", "India", 2007, 23000));
    listOfCompany.add(new RevenueReport("CTS", "USA", 2010, 42000));
    listOfCompany.add(new RevenueReport("Accenture", "France", 2008, 15000));
    listOfCompany.add(new RevenueReport("Air India", "India", 2005, 10000));
    listOfCompany.add(new RevenueReport("Kingfisher", "India", 2011, 8000));
    listOfCompany.add(new RevenueReport("Vodaphone", "Netharland", 2006, 45000));
    
    return listOfCompany;
  }
  
  public static List<AirPollution> getAirPollutionSource() throws IOException {
    
    List<AirPollution> airPollutionList = new LinkedList<AirPollution>();
    String fileName = "AirPollution.csv";
    
    InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    
    //File targetFile = new File("C:/AirPollution.xml");
    //FileUtils.copyInputStreamToFile(is, targetFile);
    //finally of FileUtils close stream
    try {
      CSVReader reader = new CSVReader(br, ',', '\"', 1);
      String[] row = null;
      while ((row = reader.readNext()) != null) {
        for (String s : row) {
          System.out.println(s);
        }
        
        airPollutionList.add(new AirPollution(row[0], Integer.parseInt(row[1])));
      }
      reader.close();
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }
    return airPollutionList;
  }
}
