Storing the Results

A servlet can store data for JSP pages in three main places:
- in the HttpServletRequest ,
- in the HttpSession ,
- in the ServletContext .

These storage locations correspond to the three nondefault values of the scope attribute of
jsp:useBean : that is, request , session , and application .

Forwarding Requests to JSP Pages

Redirecting Instead of Forwarding

Here is a summary of the behavior of forward .
• Control is transferred entirely on the server. No network traffic is involved.
• The user does not see the address of the destination JSP page and pages can be placed in WEB-INF to prevent
the user from accessing them without going through the servlet that sets up the data.
This is beneficial if the JSP page makes sense only in the context of servlet-generated data.

Here is a summary of sendRedirect .
• Control is transferred by sending the client a 302 status code and a Location response header.
Transfer requires an additional network round trip.
• The user sees the address of the destination page and can bookmark it and access it independently.
This is beneficial if the JSP is designed to use default values when data is missing.

For example, this approach would be used when redisplaying an incomplete HTML form or summarizing the contents of a shopping cart. In both cases, previously created data would be extracted from the user’s session, so the JSP page makes sense even for requests that do not involve the servlet.


15.4 Interpreting Relative URLs in the Destination Page.
Although a servlet can forward the request to an arbitrary location on the same server, the process is quite different from that of using the sendRedirect method of HttpServletResponse .
- First, sendRedirect requires the client to reconnect to the new resource, whereas the forward method of RequestDispatcher is handled completely on the server.

- Second, sendRedirect does not automatically preserve all of the request data; forward does.

- Third, sendRedirect results in a different final URL, whereas with forward , the URL of the original servlet is
maintained.

