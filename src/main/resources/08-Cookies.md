Topics
• Understanding the benefits and drawbacks of cookies
• Sending outgoing cookies
• Receiving incoming cookies
• Tracking repeat visitors
• Specifying cookie attributes
• Differentiating between session cookies and persistent cookies
• Simplifying cookie usage with utility classes
• Modifying cookie values
• Remembering user preferences

8.1 Benefits of Cookies
There are four typical ways in which cookies can add value to your site.
• Identifying a user during an e-commerce session.
This type of short-term tracking is so important that another API is layered on top of cookies for this purpose.

• Remembering usernames and passwords.
Cookies let a user log in to a site automatically, providing a significant convenience for users of unshared computers.

• Customizing sites.
Sites can use cookies to remember user preferences.

• Focusing advertising
Cookies let the site remember which topics interest certain users and show advertisements relevant to those interests.

8.2 Some Problems with Cookies.

8.3 Deleting Cookies.

8.4 Sending and Receiving Cookies.

8.5 Using Cookies to Detect First-Time Visitors.

8.6 Using Cookie Attributes.

8.7 Differentiating Session Cookies from Persistent Cookies.

8.8 Basic Cookie Utilities.
- Finding Cookies with Specified Names.
- Creating Long-Lived Cookies.

8.9 Putting the Cookie Utilities into Practice.

8.10 Modifying Cookie Values: Tracking User Access Counts.

8.11 Using Cookies to Remember User Preferences.
One of the most common applications of cookies is to use them to “remember” user preferences

