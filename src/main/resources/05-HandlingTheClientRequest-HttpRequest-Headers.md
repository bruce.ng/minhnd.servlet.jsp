5.3 Understanding HTTP 1.1  Request Headers

1. Accept
This header specifies the MIME types that the browser or other clients can handle.
A servlet that can return a resource in more than one format can examine the Accept header to decide which format to use.

For example:
Images in PNG format have some compression advantages over those in GIF, but not all browsers support PNG.
If you have images in both formats, your servlet can  call request.getHeader("Accept"), check for image/png, and if it finds a match,
use blah.png filenames in all the IMG elements it generates. Otherwise, it would just use blah.gif.

2. Accept-Charset
3. Accept-Encoding: This header designates the types of encodings that the client knows how to handle
4. Accept-Language
5. Authorization
6. Connection:
	This header indicates whether the client can handle persistent HTTP connections Persistent connections permit the client or 
other browser to retrieve multiple files (e.g., an HTML file and several associated images) with a single socket connection,
thus saving the overhead of negotiating several independent connections.
- With an HTTP 1.1 request, persistent connections are the default, and the client must specify a value of close for this header to use old-style connections.
- In HTTP 1.0, a value of Keep-Alive means that persistent connections should be used

7. Content-Length
This header is applicable only to POSTrequests and gives the size of the POSTdata in bytes. Rather than calling request.getIntHeader("Content-Length"), you can simply use request.getContentLength().

8. Cookie
This header returns cookies to servers that previously sent them to the browser. Never read this header directly because doing so would require cumbersome low-level parsing. use request.getCookies() instead.

9. Host

10. If-Modified-Since
	This header indicates that the client wants the page only if it has been changed after the specified date. The server sends a 304 (Not Modified) header if no newer result is available.
This option is useful because it lets browsers cache documents and reload them over the network only when they’ve changed.

11. If-Unmodified-Since
	This header is the reverse of If-Modified-Since; it specifies that the operation should succeed only if the document is older than the specified date. Typically, If-Modified-Sinceis used for GETrequests (“give me the document only if it is newer than my cached version”), whereas If-Unmodified-Since is used for PUTrequests (“update this document only if nobody else has changed 
it since I generated it”). This header is new in HTTP 1.1.

12. Referer

This header indicates the URL of the referring Web page.
For example, if you are at Web page 1 and click on a link to Web page 2, the URL of Web page 1 is included in the Referer header when the browser requests Web page 2.
Most major browsers set this header, so it is a useful way of tracking where requests come from.

This capability is helpful for tracking advertisers who refer people to your site, for slightly changing content depending on the referring site, for identifying when users first enter your application, or simply for keeping track of where your traffic comes from. In the last case, most people rely on Web server log files, since the Referer is typically recorded there. Although the
Referer header is useful, don’t rely too heavily on it since it can easily be spoofed by a custom client. Also, note that, owing to a spelling mistake by one of the original HTTP authors, this header is Referer , not the expected Referrer .

13. User-Agent
This header identifies the browser or other client making the request and can be used to return different content to different types of browsers

5.4 Sending Compressed Web Pages


5.5 Differentiating Among Different Browser Types
The User-Agent header identifies the specific browser that is making the request.
Although use of this header appears straightforward at first glance, a few subtleties are involved.
	- Use User-Agent only when necessary.Otherwise, you will have difficult-to-maintain code that consists of tables of browser versions and associated capabilities.
	For example, instead of remembering that the Windows version of Internet Explorer 5 supports gzip compression but the MacOS version doesn’t, check the Accept-Encoding header. Instead of remembering which browsers support Java and which don’t, use the APPLETtag with fallback code 
between <APPLET>and </APPLET>.


5.6 Changing the Page According to How the User Got There
5.7 Accessing the Standard CGI Variables.




