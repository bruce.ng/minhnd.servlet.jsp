02-Setup-and-Config.pdf


	1. Setting the JAVA_HOME variable. Set this variable to list the base SDK installation directory.
	
	2. Specifying the server port. Edit install_dir/conf/server.xml and change the value of the port attribute of the Connector element from 8080 to 80.

	3. Enabling servlet reloading. Add a DefaultContext element to install_dir/conf/server.xml
to tell Tomcat to reload servlets that have been loaded into the server’s memory but whose class files
have changed on disk since they were loaded.

	4. Enabling the ROOT context. To enable the default Web application, uncomment the following line in install_dir/conf/server.xml .
<Context path="" docBase="ROOT" debug="0"/>

	5. Turning on the invoker servlet. To permit you to run servlets without making changes to your web.xml file, some versions of Tomcat require you to uncomment the /servlet/* servlet-mapping element in install_dir/conf/web.xml .
	
	6. Increasing DOS memory limits. On older Windows versions, tell the operating system to reserve more space for environment variables.
	
	7. Setting CATALINA_HOME . Optionally, set the CATALINA_HOME environment variable to refer to the base Tomcat installation directory.