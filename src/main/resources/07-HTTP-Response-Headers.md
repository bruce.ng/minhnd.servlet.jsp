  A response from a Web server normally consists of a status line, one or more response headers (one of which must be Content-Type),
a blank line, and the document. To get the most out of your servlets, you need to know how to use the status line and response
headers effectively, not just how to generate the document.

However, specifying headers can also play a useful role even when no unusual status code is set.

  Response headers can be used to specify cookies, to supply the page modification date (for client-side caching), to instruct the browser to reload the page after a designated interval, to give the file size so that persistent HTTP connections can be used, to designate the type of document being generated, and to perform many other tasks.

7.1 Setting Response Headers from Servlets
7.2 Understanding HTTP 1.1 Response Headers
- Allow
- Cache-Control
- Connection
- Content-Disposition
- Content-Encoding
- Content-Language
- Content-Length
- Content-Type
The Content-Type header gives the MIME (Multipurpose Internet Mail Extension) type of the response document.

Type                |          Meaning
application/pdf     |          Acrobat (.pdf) file

- Expires
- Last-Modified
- Location
- Pragma
- Refresh
- Retry-After
- Set-Cookie
- WWW-Authenticate

7.3 Building Excel Spreadsheets
7.4 Persistent Servlet State and Auto-Reloading Pages
• A way to store data between requests.
  - For data that is not specific to any one client, store it in a field (instance variable) of the servlet.
  - For data that is specific to a user, store it in the HttpSession object (“Session Tracking”).
  - For data that needs to be available to other servlets or JSP pages, store it in the ServletContext (sharing data, “Using JavaBeans Components in JSP Documents”).
  
• A way to keep computations running after the response is sent to the user.
This task is simple: just start a Thread. The thread started by the system to answer requests automatically finishes when the response is finished, but other threads can keep running. The only subtlety: set the thread priority to a low value so that you do not slow down the server.

• A way to get the updated results to the browser when they are ready.
Unfortunately, because browsers do not maintain an open connection to the server, there is no easy way for the server to proactively send the new results to the browser. Instead, the browser needs to be told to ask for updates. That is the purpose of the Refresh respose header.

7.5 Using Servlets to Generate JPEG Images.
Two main steps servlets have to perform to build multimedia content.

1. Inform the browser of the content type they are sending:
To accomplish this task, servlets set the Content-Type response header by using the setContentType method of HttpServletResponse.

2. Send the output in the appropriate format:
This format varies among document types, of course, but in most cases you send binary data, not strings as you do with HTML documents. Consequently, servlets will usually get the raw output stream by using the getOutputStream method, rather than getting a PrintWriter by using getWriter






















































